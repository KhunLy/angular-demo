import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  private _items : any[];
  public get items() : any[] {
    return this._items;
  }
  public set items(v : any[]) {
    this._items = v;
  }


  constructor() { }

  ngOnInit() {
    this.items = [
      { title: 'Demo', icon: '', children: [
        { title: 'Demo 1 - Binding One Way', link: '/demo/demo1' },
        { title: 'Demo 2 - Events', link: '/demo/demo2' },
        { title: 'Demo 3 - Binding Two Ways', link: '/demo/demo3' },
        { title: 'Demo 4 - *ngIf', link: '/demo/demo4' },
        { title: 'Demo 5 - *ngFor', link: '/demo/demo5' },
        { title: 'Demo 6 - Models', link: '/demo/demo6' },
      ] },
      { title: 'Exercices', icon: '', children: [
        { title: 'Ex 1 - Timer', link: '/ex/ex1' },
        { title: 'Ex 2 - Shopping List', link: '/ex/ex2' }
      ] }
    ];
  }

}
