import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbThemeModule, NbButtonModule, NbLayoutModule, NbSidebarModule, NbCardModule, NbListModule, NbMenuModule, NbInputModule, NbIconModule, NbDialogService, NbDialogModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbInputModule,
  ],
  exports: [
    NbThemeModule,
    NbEvaIconsModule,
    NbIconModule,
    NbLayoutModule,
    NbSidebarModule,
    NbMenuModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbInputModule,
    NbDialogModule
  ]
})
export class NebularModule { }
