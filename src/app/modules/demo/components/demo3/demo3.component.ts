import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {


  private _myProp : string;
  public get myProp() : string {
    return this._myProp;
  }
  public set myProp(v : string) {
    this._myProp = v;
  }

  constructor() {
    this.myProp = 'Khun'
  }

  ngOnInit() {
  }
}
