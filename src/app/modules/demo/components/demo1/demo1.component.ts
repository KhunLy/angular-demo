import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {


  // compute prop + tab to generate a property in vsCode
  private _myProp : string;
  public get myProp() : string {
    return this._myProp;
  }
  public set myProp(v : string) {
    this._myProp = v;
  }


  constructor() {

  }

  ngOnInit() {
    this.myProp = 'World';
    // setTimeout(function() {
    //   // inside a function you lost the class scope use a lambda expression instead () => {}
    //   this.maVariable = 'Khun';
    // }, 2000);
    setTimeout(() => {
      this.myProp = 'Khun';
    }, 2000);
  }

}
