import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo5',
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss']
})
export class Demo5Component implements OnInit {

  private _items : Array<string>; // string[]
  public get items() : Array<string> {
    return this._items;
  }
  public set items(v : Array<string>) {
    this._items = v;
  }

  constructor() {
    // initialisation de la liste dans le constructeur
    this.items = [
      'Sel',
      'Poivre',
      'Sucre',
    ]
  }

  ngOnInit() {
  }

}
