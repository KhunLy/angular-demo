import { Component, OnInit } from '@angular/core';
import { Product } from '../../_models/product';

@Component({
  selector: 'app-demo6',
  templateUrl: './demo6.component.html',
  styleUrls: ['./demo6.component.scss']
})
export class Demo6Component implements OnInit {


  private _items : Product[];
  public get items() : Product[] {
    return this._items;
  }
  public set items(v : Product[]) {
    this._items = v;
  }


  constructor() {
    // <interface>{} Specifies that the oject implements the interface
    this.items = [
      <Product>{
        id: 1,
        name: 'Coca Cola',
        price: 2,
        discount: 25,
        picture: 'coca-cola.jpg'
      },
      <Product>{
        id: 2,
        name: 'Fanta',
        price: 1.9,
        discount: null,
        picture: 'fanta.jpg'
      },
      <Product>{
        id: 3,
        name: 'Sprite',
        price: 1.9,
        discount: null,
        picture: 'sprite.jpg'
      },
      <Product>{
        id: 4,
        name: 'Dr Pepper',
        price: 2.2,
        discount: 10,
        picture: 'dr-pepper.jpg'
      },
      <Product>{
        id: 5,
        name: 'Gini',
        price: 2.2,
        discount: null,
        picture: 'gini.jpg'
      },
    ]
  }

  ngOnInit() {
  }

}
