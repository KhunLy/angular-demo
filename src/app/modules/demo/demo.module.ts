import { NebularModule } from './../nebular/nebular.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import { Demo1Component } from './components/demo1/demo1.component';
import { Demo2Component } from './components/demo2/demo2.component';
import { Demo3Component } from './components/demo3/demo3.component';
import { FormsModule } from '@angular/forms';
import { Demo4Component } from './components/demo4/demo4.component';
import { Demo5Component } from './components/demo5/demo5.component';
import { Demo6Component } from './components/demo6/demo6.component';
import { NbDialogModule } from '@nebular/theme';


@NgModule({
  declarations: [DemoComponent, Demo1Component, Demo2Component, Demo3Component, Demo4Component, Demo5Component, Demo6Component],
  imports: [
    CommonModule,
    DemoRoutingModule,
    FormsModule,
    NebularModule,
    NbDialogModule.forChild()
  ]
})
export class DemoModule { }
