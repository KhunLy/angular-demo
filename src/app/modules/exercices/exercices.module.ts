import { NebularModule } from './../nebular/nebular.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExercicesRoutingModule } from './exercices-routing.module';
import { ExercicesComponent } from './exercices.component';
import { Ex1Component } from './components/ex1/ex1.component';
import { FormsModule } from '@angular/forms';
import { Ex2Component } from './components/ex2/ex2.component';
import { ConfirmDialogBoxComponent } from './components/confirm-dialog-box/confirm-dialog-box.component';
import { NbDialogModule } from '@nebular/theme';


@NgModule({
  declarations: [ExercicesComponent, Ex1Component, Ex2Component, ConfirmDialogBoxComponent],
  imports: [
    CommonModule,
    ExercicesRoutingModule,
    FormsModule,
    NebularModule,
    NbDialogModule.forChild()
  ],
  entryComponents: [
    ConfirmDialogBoxComponent
  ]
})
export class ExercicesModule { }
