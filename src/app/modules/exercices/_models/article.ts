export interface Article {
  name: string;
  isChecked: boolean;
  quantity: number;
}
