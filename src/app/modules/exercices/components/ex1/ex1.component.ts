import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {
  private _value : number;
  public get value() : number {
    return this._value;
  }
  public set value(v : number) {
    this._value = v;
  }


  private _isStarted : boolean;
  public get isStarted() : boolean {
    return this._isStarted;
  }
  public set isStarted(v : boolean) {
    this._isStarted = v;
  }


  // par défaut: private pour les variables
  _timer: any;


  constructor() {
    this.value = 0;
  }

  ngOnInit() {
    // setInterval(() => {
    //   this.value++;
    // }, 1000);
  }

  start() {
    this._timer = setInterval(() => {
      this.value++;
    }, 1000);
    this._isStarted = true;
  }

  stop() {
    clearInterval(this._timer);
    this.isStarted = false;
  }

  reset() {
    this.value = 0;
    this.stop();
  }

}
