import { Article } from './../../_models/article';
import { Component, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { ConfirmDialogBoxComponent } from '../confirm-dialog-box/confirm-dialog-box.component';

@Component({
  selector: 'app-ex2',
  templateUrl: './ex2.component.html',
  styleUrls: ['./ex2.component.scss']
})
export class Ex2Component implements OnInit {

  private _input : string;
  public get input() : string {
    return this._input;
  }
  public set input(v : string) {
    this._input = v;
  }

  public get inputValid(): boolean {
    return this.input != null && this.input != '';
  }


  private _articles : Article[];
  public get articles() : Article[] {
    return this._articles;
  }
  public set articles(v : Article[]) {
    this._articles = v;
  }


  constructor(private dialogService: NbDialogService) {
    this.articles = [];
  }

  ngOnInit() {
  }

  onKeyDown($event: KeyboardEvent){
    if($event.key == 'Enter'){
      this.add();
    }
  }

  add() {
    if(this.inputValid){
      let currentArticle = this.articles.find(a => a.name == this.input.toLowerCase().trim());
      if(currentArticle == null){
        this.articles.push({name: this.input.trim().toLowerCase(), quantity: 1, isChecked: false});
      }
      else{
        currentArticle.quantity++;
      }

      this.input = null;
    }
  }

  check(item: Article) {
    item.isChecked = !item.isChecked;
  }

  openConfirmDialogBox(item: Article) {
    let dialogRef = this.dialogService.open(ConfirmDialogBoxComponent);
    dialogRef.onClose.subscribe(data => {
      if(data) {
        this.articles.splice(this.articles.indexOf(item), 1);
      }
    })
  }
}
