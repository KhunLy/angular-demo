import { NebularModule } from './modules/nebular/nebular.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NavComponent } from './components/nav/nav.component';
import { NbDialogModule, NbThemeModule, NbSidebarModule, NbMenuModule } from '@nebular/theme';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
  ],
  imports: [
    BrowserModule,
    // module that configures routing
    AppRoutingModule,
    BrowserAnimationsModule,
    // import all base Nebular Modules
    // need this module for binding 2 ways
    FormsModule,
    NebularModule,
    NbThemeModule.forRoot({ name: 'cosmic' }),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDialogModule.forRoot()
  ],
  providers: [],
  // tells that the app-root is AppComponent
  bootstrap: [AppComponent]
})
export class AppModule { }
